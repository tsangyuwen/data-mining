import os     
os.environ["PATH"] += os.pathsep + 'C:\\Users\\yuwen\\Anaconda3\\Library\\bin\\graphviz'

from sklearn import tree
from sklearn import preprocessing
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import pydotplus
import xlsxwriter
import matplotlib.pyplot as plt
import math

# 讀取原始資料
test_dataset = np.loadtxt('./segmentation.data.txt', dtype = 'str', delimiter  = ',')
train_dataset = np.loadtxt('./segmentation.test.txt', dtype = 'str', delimiter  = ',')

# 合併測試資料集及
original_dataset = np.concatenate((test_dataset, train_dataset), axis = 0)

# 指定class及feature屬性
class_attr = original_dataset[:, 0]
feature_attr = original_dataset[:, 1::].astype('float')

# 列出屬性名稱
feature_names = np.array(['REGION-CENTROID-COL', 'REGION-CENTROID-ROW', 'REGION-PIXEL-COUNT', 
                          'SHORT-LINE-DENSITY-5', 'SHORT-LINE-DENSITY-2', 'VEDGE-MEAN', 'VEDGE-SD', 
                          'HEDGE-MEAN', 'HEDGE-SD', 'INTENSITY-MEAN', 'RAWRED-MEAN', 'RAWBLUE-MEAN',
                          'RAWGREEN-MEAN', 'EXRED-MEAN', 'EXBLUE-MEAN', 'EXGREEN-MEAN', 'VALUE-MEAN', 
                          'SATURATION-MEAN', 'HUE-MEAN'])

# 切割訓練及測試資料
class_attr_train, class_attr_test, feature_attr_train, feature_attr_test = train_test_split(class_attr, feature_attr, test_size = 0.3)
origin_feature_attr_test = feature_attr_test

## 資料預處理
# 採用標準化的方式進行預處理
scale = preprocessing.StandardScaler()
# 計算平均值和標準差
scale.fit(feature_attr_train)
# 資料標準化
feature_attr_train = scale.transform(feature_attr_train)
feature_attr_test = scale.transform(feature_attr_test)

# 生成圖片
def create_picture(x_ticks, y_ticks):
    plt.figure()
    plt.xticks(x_ticks)
    plt.yticks(y_ticks)
    
# 輸出圖片
def export_picture(title, xlabel, file_name):
    # 設定圖片標題
    plt.title(title)
    # 設定x軸、y軸標題
    plt.xlabel(xlabel)
    plt.ylabel('accuracy')
    # 設定圖例
    plt.legend(loc = 'best')
    # 儲存圖片
    plt.savefig(file_name)
    
# 產出預測資料並計算正確率  
def predict_acc(classifer, feature_attr, class_attr):
    predict = classifer.predict(feature_attr)
    return metrics.accuracy_score(class_attr, predict)
   
    
## 方法一 - Decision Tree  
# 決定決策樹算法並訓練
def train_decision_tree(crit, depth):
    # 採取決策樹演算法
    decision_tree_classifier = DecisionTreeClassifier(criterion = crit, max_depth = depth)
    # 訓練決策樹
    return decision_tree_classifier.fit(feature_attr_train, class_attr_train)

    
# 紀錄決策樹的正確率
gini_decision_tree_train_acc_list = []
gini_decision_tree_test_acc_list = []
entropy_decision_tree_train_acc_list = []
entropy_decision_tree_test_acc_list = []

# 深度範圍
depth_range = range(2, 31)

# 計算決策樹的最佳深度
for depth in depth_range:
    gini_decision_tree = train_decision_tree('gini', depth)
    entropy_decision_tree = train_decision_tree('entropy', depth)
    
    # 紀錄訓練集測試資料的正確率
    gini_decision_tree_train_acc_list.append(predict_acc(gini_decision_tree, feature_attr_train, class_attr_train))
    entropy_decision_tree_train_acc_list.append(predict_acc(entropy_decision_tree, feature_attr_train, class_attr_train))
    
    gini_decision_tree_test_acc_list.append(predict_acc(gini_decision_tree, feature_attr_test, class_attr_test))
    entropy_decision_tree_test_acc_list.append(predict_acc(entropy_decision_tree, feature_attr_test, class_attr_test))

# 最高正確率
gini_decision_tree_max_acc = max(gini_decision_tree_test_acc_list)
entropy_decision_tree_max_acc = max(entropy_decision_tree_test_acc_list)

# 正確率最高的深度
gini_decision_tree_max_acc_depth = gini_decision_tree_test_acc_list.index(gini_decision_tree_max_acc) + 1
entropy_decision_tree_max_acc_depth = entropy_decision_tree_test_acc_list.index(entropy_decision_tree_max_acc) + 1

print("決策樹")
print("gini決策樹正確率：" + str(gini_decision_tree_max_acc) + "\ngini正確率最高深度：" + str(gini_decision_tree_max_acc_depth))
print("entropy決策樹正確率：" + str(entropy_decision_tree_max_acc) + "\nentropy正確率最高深度：" + str(entropy_decision_tree_max_acc_depth))


## 畫出決策樹
# 訓練最高正確率的決策樹
if gini_decision_tree_max_acc > entropy_decision_tree_max_acc:
    max_acc_decision_tree = train_decision_tree('gini', gini_decision_tree_max_acc_depth)
else:
    max_acc_decision_tree = train_decision_tree('entropy', entropy_decision_tree_max_acc_depth)
    
# 繪製決策樹(DOT格式)
dot_data = tree.export_graphviz(max_acc_decision_tree, feature_names = feature_names, class_names = class_attr, filled = True)
# 取得圖片(DOT格式)
graph = pydotplus.graph_from_dot_data(dot_data)
# 轉存為pdf格式
graph.write_pdf('./decision_tree.pdf')


## 產出比較決策樹圖片
# 用gini算法訓練與測試正確率比較
create_picture(np.arange(2, 32, step = 2), np.arange(0.1, 1.2, step = 0.1))

plt.plot(depth_range, gini_decision_tree_train_acc_list, marker = 'o', label = 'train')
plt.plot(depth_range, gini_decision_tree_test_acc_list, marker = 'o', label = 'test')

export_picture('Gini Decision Tree', 'the depth of decision tree', './GiniDecisionTree.png')

# 用entropy算法訓練與測試正確率比較
create_picture(np.arange(2, 32, step = 2), np.arange(0.1, 1.2, step = 0.1))

plt.plot(depth_range, entropy_decision_tree_train_acc_list, marker = 'o', label = 'train')
plt.plot(depth_range, entropy_decision_tree_test_acc_list, marker = 'o', label = 'test')

export_picture('Entropy Decision Tree', 'the depth of decision tree', './EntropyDecisionTree.png')

# 比較gini與entropy算法的正確率
create_picture(np.arange(2, 32, step = 2), np.arange(0.1, 1.2, step = 0.1))

plt.plot(depth_range, gini_decision_tree_test_acc_list, marker = 'o', label = 'gini')
plt.plot(depth_range, entropy_decision_tree_test_acc_list, marker = 'o', label = 'entropy')

export_picture('Compare Gini and Entropy Decision Tree', 'the depth of decision tree', './CompareDecisionTree.png')


## 方法二 - K_Nearest_Neighbors
# 決定決策樹算法並訓練
def train_knn(k, power):
    # 採取最近鄰居法演算法
    knn_classifier = KNeighborsClassifier(n_neighbors = k, weights = 'distance', algorithm = 'auto', p = power)
    # 訓練決策樹
    return knn_classifier.fit(feature_attr_train, class_attr_train)

# 鄰居個數範圍
k_range = range(1, 21)

# 紀錄最近鄰居法的正確率
l1_knn_train_acc_list = []
l2_knn_train_acc_list = []
l1_knn_test_acc_list = []
l2_knn_test_acc_list = []

# 計算最近鄰居法的最佳鄰居個數
for k in k_range:
    l1_knn = train_knn(k, 1)
    l2_knn = train_knn(k, 2)
    
    # 紀錄訓練集測試資料的正確率
    l1_knn_train_acc_list.append(predict_acc(l1_knn, feature_attr_train, class_attr_train))
    l2_knn_train_acc_list.append(predict_acc(l2_knn, feature_attr_train, class_attr_train))
    
    l1_knn_test_acc_list.append(predict_acc(l1_knn, feature_attr_test, class_attr_test))
    l2_knn_test_acc_list.append(predict_acc(l2_knn, feature_attr_test, class_attr_test))
    
# 最高正確率
l1_knn_max_acc = max(l1_knn_test_acc_list)
l2_knn_max_acc = max(l2_knn_test_acc_list)

# 正確率最高的深度
l1_knn_max_acc_k = l1_knn_test_acc_list.index(l1_knn_max_acc) + 1
l2_knn_max_acc_k = l2_knn_test_acc_list.index(l2_knn_max_acc) + 1

print("\n最近鄰居法")
print("L1最近鄰居法正確率：" + str(l1_knn_max_acc) + "\nL1正確率最高深度：" + str(l1_knn_max_acc_k))
print("L2最近鄰居法正確率：" + str(l2_knn_max_acc) + "\nL2正確率最高深度：" + str(l2_knn_max_acc_k))

# 訓練最高正確率的決策樹
if l1_knn_max_acc > l2_knn_max_acc:
    max_acc_knn = train_knn(l1_knn_max_acc_k, 1)
else:
    max_acc_knn = train_knn(l2_knn_max_acc_k, 2)


## 產出比較最近鄰居法圖片
# 用L1算法訓練與測試正確率比較
create_picture(np.arange(1, 21, step = 1), np.arange(0.91, 1.03, step = 0.02))

plt.plot(k_range, l1_knn_train_acc_list, marker = 'o', label = 'train')
plt.plot(k_range, l1_knn_test_acc_list, marker = 'o', label = 'test')

export_picture('L1 K Nearest Neighbors', 'the number of k', './L1KNN.png')

# 用L2算法訓練與測試正確率比較
create_picture(np.arange(1, 21, step = 1), np.arange(0.91, 1.03, step = 0.02))

plt.plot(k_range, l2_knn_train_acc_list, marker = 'o', label = 'train')
plt.plot(k_range, l2_knn_test_acc_list, marker = 'o', label = 'test')

export_picture('L2 K Nearest Neighbors', 'the number of k', './L2KNN.png')

# 比較L1與L2算法的正確率
create_picture(np.arange(1, 21, step = 1), np.arange(0.91, 1.03, step = 0.02))

plt.plot(k_range, l1_knn_test_acc_list, marker = 'o', label = 'L1')
plt.plot(k_range, l2_knn_test_acc_list, marker = 'o', label = 'L2')

export_picture('Compare L1 and L2 K Nearest Neighbors', 'the number of k', './CompareKNN.png')


## 輸出成excel
def trans_class(class_attr):
    return class_attr.reshape(class_attr.shape[0], 1)

# 寫入標題
def write_name(worksheet, names):
    col = 0
    for name in names:
        worksheet1.write(0, col, name)
        col += 1

# 寫入資料
def write_data(worksheet, result):
    for row, data in enumerate(result):
        worksheet.write_row(row + 1, 0, data)

#設定Excel檔的原始類別欄位
# export xls
arrow = np.full(((class_attr_test.shape[0]), 1), '→')
empty = np.full(((class_attr_test.shape[0]), 1), ' ')
# 標題名稱
names = np.concatenate((['class', ' ', 'predict', ' '], feature_names), axis = 0)
# 正確類別
original_class = trans_class(class_attr_test)

# decision tree預測類別
predict_class = trans_class(max_acc_decision_tree.predict(feature_attr_test))

decision_tree_result = np.concatenate((original_class, arrow, predict_class, empty, origin_feature_attr_test), axis = 1)

# knn預測類別
predict_class = trans_class(max_acc_knn.predict(feature_attr_test))

knn_result = np.concatenate((original_class, arrow, predict_class, empty, origin_feature_attr_test), axis = 1)

workbook = xlsxwriter.Workbook('./ty.xlsx')

worksheet1 = workbook.add_worksheet('Decision Tree')
write_name(worksheet1, names)
write_data(worksheet1, decision_tree_result)

worksheet2 = workbook.add_worksheet('K Nearest Neighbors')
write_name(worksheet2, names)
write_data(worksheet2, knn_result)
    
workbook.close()
