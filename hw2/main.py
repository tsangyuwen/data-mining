# import os     
# os.environ["PATH"] += os.pathsep + 'E:\\nltk_data'

from sklearn import datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn import metrics
from sklearn.model_selection import cross_validate
from nltk.corpus import stopwords
import tarfile
import numpy as np
import matplotlib.pyplot as plt

# 解壓縮
tar = tarfile.open("mini_newsgroups.tar.gz", "r:gz")
tar.extractall()
tar.close()

# 原始資料集
original_dataset = datasets.load_files('mini_newsgroups')

# 設定stopwords為英文
stopwords = set(stopwords.words('english'))

# 轉換編碼方式、並記錄頻率
tfidf = TfidfVectorizer(encoding = 'ISO-8859-1', stop_words = stopwords)
trans_dataset = tfidf.fit_transform(original_dataset.data).toarray()

# 打散資料順序
indices = np.random.permutation(trans_dataset.shape[0])

# 切割訓練及測試資料
feature_attr = trans_dataset[indices]
class_attr = original_dataset.target[indices]
    
# 輸出圖片
def export_picture(title, xlabel, file_name):
    # 設定圖片標題
    plt.title(title)
    # 設定x軸、y軸標題
    plt.xlabel(xlabel)
    plt.ylabel('accuracy')
    # 設定圖例
    plt.legend(loc = 'best')
    # 儲存圖片
    plt.savefig(file_name)
    
# 產出預測資料並計算正確率  
def predict_acc(classifer, feature_attr, class_attr):
    predict = classifer.predict(feature_attr)
    return metrics.accuracy_score(class_attr, predict)
   
    
## 方法一 - naive_bayes
# 決定naive_bayes算法並訓練
def train_naive_bayes(alpha):
    # 採取naive_bayes演算法
    naive_bayes = MultinomialNB(alpha, fit_prior = False, class_prior = None)
    # 訓練naive_bayes
    return cross_validate(naive_bayes, feature_attr, class_attr, cv = 5, return_train_score = True)

# 紀錄naive_bayes的正確率
naive_bayes_train_acc_list = []
naive_bayes_test_acc_list = []

# alpha範圍
alpha_range = np.arange(1, 10, step = 1)

# 計算naive_bayes的最佳alpha
for alpha in alpha_range:
    naive_bayes = train_naive_bayes(alpha)
    
    # 紀錄訓練集測試資料的正確率
    naive_bayes_train_acc_list.append(naive_bayes['train_score'].mean())
    naive_bayes_test_acc_list.append(naive_bayes['test_score'].mean())

# 最高正確率
naive_bayes_max_acc = max(naive_bayes_test_acc_list)
train = max(naive_bayes_train_acc_list)

# 正確率最高的alpha
naive_bayes_max_acc_alpha = naive_bayes_test_acc_list.index(naive_bayes_max_acc) + 1

print("NAIVE BAYES")
print("NAIVE BAYES正確率：" + str(naive_bayes_max_acc) + "\nNAIVE BAYES正確率最高alpha：" + str(naive_bayes_max_acc_alpha))   
print("NAIVE BAYES train 正確率：" + str(train))
## 產出比較NAIVE BAYES訓練及測試正確率圖片
# create_picture(alpha_range, np.arange(0.5, 1.1, step = 0.1))
plt.figure()

plt.plot(alpha_range, naive_bayes_train_acc_list, marker = 'o', label = 'train')
plt.plot(alpha_range, naive_bayes_test_acc_list, marker = 'o', label = 'test')

export_picture('naive_bayes', 'the alpha of naive_bayes', './naive_bayes.png')


## 方法二 - SVM
# 決定SVM算法並訓練
def train_svm(c):
    # 採取SVM演算法
    svm = LinearSVC(penalty = 'l2', loss = 'squared_hinge', dual = True, C = c, multi_class = 'ovr', fit_intercept = True, max_iter = 1000)
    # 訓練SVM
    return cross_validate(svm, feature_attr, class_attr, cv = 5, return_train_score = True)

# c範圍
c_range = np.arange(0.1, 1, step = 0.1)

# 紀錄SVM的正確率
svm_train_acc_list = []
svm_test_acc_list = []

# 計算SVM的最佳c個數
for c in c_range:
    svm = train_svm(c)
    
    # 紀錄訓練集測試資料的正確率
    svm_train_acc_list.append(svm['train_score'].mean()) 
    svm_test_acc_list.append(svm['test_score'].mean())
    
# 最高正確率
svm_max_acc = max(svm_test_acc_list)
train = max(svm_train_acc_list)

# 正確率最高的c
svm_max_acc_iter = svm_test_acc_list.index(svm_max_acc) / 10

print("\nSVM")
print("SVM正確率：" + str(svm_max_acc) + "\nSVM正確率最高c：" + str(svm_max_acc_iter))
print("SVM train 正確率：" + str(train))

## 產出比較SVM訓練及測試正確率圖片
plt.figure()

plt.plot(c_range, svm_train_acc_list, marker = 'o', label = 'train')
plt.plot(c_range, svm_test_acc_list, marker = 'o', label = 'test')

export_picture('svm', 'the number of c', './svm.png')
